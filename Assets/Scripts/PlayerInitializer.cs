using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerInitializer : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        gameObject.name = "Player " + photonView.Owner.ActorNumber;
        if (photonView.Owner.IsMasterClient)
        {
            gameObject.name += " Master";
        }
    }
}
