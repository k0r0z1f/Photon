using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.Demo.Cockpit;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private Transform[] spawnPositions;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private Transform bulletContainer;
    [SerializeField] private Transform enemyContainer;

    private int index;
    
    public static GameController Instance { get; private set; }

    public Transform GetBulletContainer()
    {
        return bulletContainer;
    }
    void Start()
    {
        Debug.Assert(Instance == null);
        Instance = this;
        if (!PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene("Lobby");
            
            Debug.Log("You have been redirected to the Lobby.");
            return;
        }

        index = PhotonNetwork.CurrentRoom.PlayerCount - 1;
        PhotonNetwork.Instantiate(playerPrefab.name, spawnPositions[index].position, spawnPositions[index].rotation);

        // if (PhotonNetwork.IsConnected)
        // {
        //     SpawnEnemies(5);
        // }
        if (PhotonNetwork.IsMasterClient)
        {
            SpawnEnemies(5);
        }
        Debug.Log("Actor number: " + PhotonNetwork.LocalPlayer.ActorNumber);
        Debug.Log("ID: " + PhotonNetwork.LocalPlayer.UserId);
    }

    public void Disconnect()
    {
        Debug.Log("Manually calling disconnect.");
        
        PhotonNetwork.Disconnect();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("OnDisconnected.");
        
        base.OnDisconnected(cause);
        
        SceneManager.LoadScene("Lobby");
    }

    private void OnDestroy()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
        }
    }
    
    private void SpawnEnemies(int nbEnemies)
    {
        for(int i = 0; i < nbEnemies; ++i)
        {
            Vector3 position = new Vector3(Random.Range(-5f, 5f), 0f, Random.Range(-5f, 5f));
            Quaternion rotation = Quaternion.Euler(0f, Random.Range(-5f, 5f), 0f);

            var o = PhotonNetwork.InstantiateRoomObject(enemyPrefab.name, position, rotation);
            //o.transform.SetParent(enemyContainer);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        if (PhotonNetwork.IsMasterClient)
        {
            SpawnEnemies(1);
        }
    }
}
