using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class DebugInfos : MonoBehaviour
{
    [SerializeField] private Text text;
    void Update()
    {
        if(PhotonNetwork.IsConnected && PhotonNetwork.CurrentRoom != null)
        {
            text.text = "Room name: " + PhotonNetwork.CurrentRoom.Name + "\n";
            text.text += "Room max players: " + PhotonNetwork.CurrentRoom.MaxPlayers + "\n";
            text.text += "Room player count: " + PhotonNetwork.CurrentRoom.PlayerCount;
        }
        else
        {
            text.text = "Not connected...";
        }
    }
}
