using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerController : MonoBehaviourPunCallbacks
{
    [SerializeField] private float speed = 3f;
    [SerializeField] private float turnSpeed = 150f;
    private float h;
    private float v;

    [SerializeField] private GameObject bullet = null;
    [SerializeField] private Transform bulletSpawnPosition;
    
    private void Start()
    {
        ChangeColor();
    }

    void Update()
    {
        if(photonView.IsMine)
        {
            h = Input.GetAxis("Horizontal");
            v = Input.GetAxis("Vertical");

            transform.Rotate(0f, h * Time.deltaTime * turnSpeed, 0f);
            transform.Translate(0f, 0f, v * Time.deltaTime * speed);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                photonView.RPC(nameof(Shoot), RpcTarget.All);
            }
        }
    }
    
    private void ChangeColor()
    {
        if (photonView.IsMine)
        {
            GetComponent<MeshRenderer>().material.color = Color.blue;
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
        }
    }

    [PunRPC]
    private void Shoot() 
    {
        GameObject newBullet = Instantiate(bullet, bulletSpawnPosition.position, bulletSpawnPosition.rotation);
        newBullet.GetComponent<Bullet>().IsMine = photonView.IsMine;
        newBullet.transform.SetParent(GameController.Instance.GetBulletContainer());
        Destroy(newBullet, 2f);
    }
}
