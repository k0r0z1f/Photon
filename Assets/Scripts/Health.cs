using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviourPun
{
    [SerializeField] private Text text;
    [SerializeField] private RectTransform healthbar = null;
    [SerializeField] private bool destroyOnDeath = false;

    private const int MAX_HEALTH = 100;
    
    private int currentHealth = 100;

    private int CurrentHealth
    {
        get
        {
            return currentHealth;
        }
        set
        {
            currentHealth = value;
            text.text = currentHealth.ToString();
            healthbar.sizeDelta = new Vector2(currentHealth, healthbar.sizeDelta.y);
        }
    }
    
    private Vector3 startPosition = Vector3.zero;

    private void Start()
    {
        startPosition = transform.position;
    }

    public void TakeDamage(int damage)
    {
        photonView.RPC(nameof(TakeDamageRPC), RpcTarget.All, damage);
    }

    [PunRPC]
    private void TakeDamageRPC(int damage)
    {
        CurrentHealth -= damage;

        if (CurrentHealth <= 0)
        {
            if (destroyOnDeath)
            {
                if (photonView.IsMine)
                {
                    PhotonNetwork.Destroy(photonView);
                }
            }
            else
            {
                Respawn();
            }
        }
    }

    private void Respawn()
    {
        CurrentHealth = MAX_HEALTH;
        if (photonView.IsMine)
        {
            transform.position = startPosition;
        }
    }
}
